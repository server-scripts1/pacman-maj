# What about downloading client's pkg update ahead of time ?

## Overview

The __pacman-maj__ idea is to download the packages updates using your __client database__ through the __cache server__ making the next client's pacman update faster because of they locally availability.

## Client database synchronization
Because the downloaded package are not installed in the server, the client database on the server will not be updated by the process. That's why there is the pacman hook __cli-sync-db-server.hook__ on the client side, witch __rsync__ his database with the server's one on each update.

## Requirement
* rsync is installed on both side (needed by the pacman hook)
* the pacman hook is installed in the client
* your client /var/lib/pacman/{local,sync} was been rsync to your server

## Configuration
There is a configuration file in the repo called __pacman-maj.conf__.

## Automatize
The repo also contain a systemd service file __pacman-maj.service__ witch can be use for scheduling the launch of the script.
